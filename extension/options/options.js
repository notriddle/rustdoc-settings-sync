if (!window.browser) window.browser = chrome;

function updateStoreSettings() {
    var storeSettings = {};

    const checkboxSettings = document.querySelectorAll(".rustdoc-settings input[type='checkbox']");
    for (let checkbox of checkboxSettings) {
      storeSettings[checkbox.id] = checkbox.checked ? 'true' : 'false';
    }
    const radioSettings = document.querySelectorAll(".rustdoc-settings input[type='radio']");
    for (let radio of radioSettings) {
      if (radio.name === "rustdoc-theme" && radio.value === "system preference" && radio.checked) {
        storeSettings["rustdoc-use-system-theme"] = "true";
      } else if (radio.checked) {
        if (radio.name === "rustdoc-theme") {
          storeSettings["rustdoc-use-system-theme"] = "false";
        }
        storeSettings[radio.name] = radio.value;
      }
    }
    if (storeSettings.hasOwnProperty("rustdoc-use-system-theme") && storeSettings["rustdoc-use-system-theme"] === "true") {
        const mql = window.matchMedia("(prefers-color-scheme: dark)");
        if (mql.matches) {
            storeSettings["rustdoc-theme"] = storeSettings["rustdoc-preferred-dark-theme"];
        } else {
            storeSettings["rustdoc-theme"] = storeSettings["rustdoc-preferred-light-theme"];
        }
    }
    const textSettings = document.querySelectorAll(".rustdoc-settings input[type='text']");
    for (let textbox of textSettings) {
      if (textbox.value.endsWith("/std/index.html")) {
        textbox.value = textbox.value.replace(/\/std\/index.html$/, '');
      } else if (textbox.value.endsWith("/")) {
        textbox.value = textbox.value.replace(/\/$/, '');
      }
      storeSettings[textbox.id] = textbox.value;
    }

    browser.storage.sync.set(storeSettings);
    browser.runtime.sendMessage({
        ty: "rses:pushAllLocalStorage",
        all: storeSettings,
    })
}

// Refresh visibility settings for theme pickers
function updateThemePickerVisibility() {
    let jsUseSystemTheme = document.getElementById("js-rustdoc-use-system-theme");
    if (jsUseSystemTheme) {
        const disp = jsUseSystemTheme.checked ? "block" : "none";
        document.getElementById("rustdoc-preferred-light-theme").style.display = disp;
        document.getElementById("rustdoc-preferred-dark-theme").style.display = disp;
    }
}

// Pull current settings
function updateUI() {
    browser.storage.sync.get(storeSettings => {
        const checkboxSettings = document.querySelectorAll(".rustdoc-settings input[type='checkbox']");
        for (let checkbox of checkboxSettings) {
            if (storeSettings.hasOwnProperty(checkbox.id)) {
                checkbox.checked = storeSettings[checkbox.id] == 'true';
            }
        }
        const radioSettings = document.querySelectorAll(".rustdoc-settings input[type='radio']");
        for (let radio of radioSettings) {
            if (radio.name === "rustdoc-theme" && storeSettings.hasOwnProperty("rustdoc-use-system-theme")) {
                if (radio.value === "system preference") {
                    radio.checked = storeSettings["rustdoc-use-system-theme"] === "true";
                    continue;
                } else if (storeSettings["rustdoc-use-system-theme"] === "true") {
                    radio.checked = false;
                    continue;
                }
            }
            if (storeSettings.hasOwnProperty(radio.name)) {
                radio.checked = radio.value === storeSettings[radio.name];
            }
        }
        const textSettings = document.querySelectorAll(".rustdoc-settings input[type='text']");
        for (let textbox of textSettings) {
            if (storeSettings.hasOwnProperty(textbox.id)) {
                textbox.value = storeSettings[textbox.id];
            }
        }
        updateThemePickerVisibility();
    });
}

updateUI();

const settings = document.querySelectorAll(".rustdoc-settings input");
for (let setting of settings) {
    setting.addEventListener("change", () => requestAnimationFrame(() => {
        updateStoreSettings();
        updateThemePickerVisibility();
    }));
}

// Respond to changes in other tabs
browser.runtime.onMessage.addListener(function(message) {
    if (!message || !message.ty) return;
    switch (message.ty) {
        case "rses:pushLocalStorage":
        case "rses:pushAllLocalStorage":
            updateUI();
            break;
        default:
            console.log("RsES received message with unknown ty: " + message.ty);
    }
});
